#!/usr/bin/env python
# coding: utf-8

# In[1]:


from copy import deepcopy
import intake
import yaml
import pandas as pd
import datetime
import json
import fsspec as fs
from copy import deepcopy


# In[2]:


def get_sources(catalog,name=None):
    newname='.'.join(
        [ a 
         for a in [name, catalog.name]
         if a
        ]
    )
    data_sources = []
    
    for key, entry in catalog.items():
        if key=="csv" or key=="esm-json":
            continue
        elif isinstance(entry, intake.catalog.Catalog):
            if newname == "main":
                newname = None
            # If the entry is a subcatalog, recursively search it
            data_sources.extend(get_sources(entry, newname))
        elif isinstance(entry, intake.source.base.DataSource):
            data_sources.append(newname+"."+key)

    return data_sources


# In[3]:


cat=intake.open_catalog("../../main.yaml")
sources=get_sources(cat)


# In[4]:


list(set([a.split('.')[1] for a in sources]))


# In[5]:


drs="levelType_dataType_frequency"


# In[6]:


catraw=yaml.safe_load(cat.text)


# In[7]:


def map_drs(olds):
    s=olds.split('.')[1]
    #description=cat.describe()
    description=catraw["sources"][s]
    metadata=description["metadata"]
    metadata_keys=["format","grid_id","member_id","institution_id","institution","references","simulation_id","variables","variable-long_names"]
    filtered={k:metadata[k] for k in metadata_keys if k in metadata.keys()}
    mapped=dict()
    mapped["format"]="netcdf"
    if filtered:
        mapped.update(filtered)
    if not "variables" in filtered:
#        if "user_parameters" in description:
        if "parameters" in description:
            mapped["variables"]=description["parameters"]["variables"]["allowed"]
#            for paramdict in description["user_parameters"]:
#                if paramdict["name"]=="variables":
#                    print(s)
#                    mapped["variables"]=paramdict["allowed"]
    for idx,k in enumerate(drs.split('_')):
        mapped[k]=s.split('_')[idx]
    if "aggregation" in mapped:
        if mapped["aggregation"].endswith('0'):
            mapped["aggregation"]+=s.split('.')[-1]
    urlpath=description["args"]["urlpath"]
    if type(urlpath)==list:
        urlpath=urlpath[0]
    if urlpath.startswith("reference"):
        mapped["format"]="zarr"
    elif urlpath.endswith(".grib") :
        mapped["format"]="grib"
    mapped["urlpath"]=description["args"]["urlpath"]
    return mapped


# In[8]:


df=pd.DataFrame(list(map(map_drs,sources)))


# In[9]:


df


# In[10]:


#df["variables"]=df["variables"].astype(str)
#df["variable-long_names"]=df["variable-long_names"].astype(str)
#df["urlpath"]=df["urlpath"].astype(str)


# In[11]:


df.to_csv("dkrz_era5_disk.csv.gz", index=False)


# In[12]:


template=json.load(fs.open("https://raw.githubusercontent.com/eerie-project/intake_catalogues/main/jasmin/jasmin-catalogue.json").open())


# In[13]:


descriptive_json=deepcopy(template)


# In[14]:


descriptive_json["assets"]['column_name']='urlpath'
del descriptive_json["assets"]['format']
descriptive_json["assets"]['format_column_name']='format'


# In[15]:


descriptive_json["attributes"]=[
    {
        'column_name': a, 'vocabulary': ''
    }
    for a in df.columns
]


# In[16]:


descriptive_json["aggregation_control"]["variable_column_name"]="variables"
#descriptive_json["aggregation_control"]["groupby_attrs"]=drs.split('.')
descriptive_json["aggregation_control"]["groupby_attrs"]=drs.split('_')
descriptive_json["aggregation_control"]["aggregations"]=[
    {
        'type': 'union',
        'attribute_name': 'variables',
        'options': {}
    }
]


# In[17]:


descriptive_json["id"]="dkrz-catalogue"
descriptive_json["last_updated"]=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
del descriptive_json["catalog_file"]


# In[18]:


#save this in github:
with open("dkrz_era5_disk.json","w") as f:
    f.write(json.dumps(
    descriptive_json,
    sort_keys=True,
    indent=4,
    separators=(',', ': ')
))


# In[19]:


esmcat=intake.open_esm_datastore(
    obj=dict(
        esmcat=descriptive_json,
        df=df
    ),
    columns_with_iterables=["variables","variable-long_names"]
)


# In[20]:


esmcat


# In[ ]:





# In[ ]:




